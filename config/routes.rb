Heatmap::Application.routes.draw do
  resources :data_categories

  resources :data
  
  resources :groups do
    member do
	  get 'leave'
	end
  end

  resources :group_requests do
    member do
	  get 'accept'
	  get 'decline'
	end
  end

  resources :group_invites do 
    member do
	  get 'accept'
	  get 'decline'
	end
  end

  resources :maps do
    member do
	  get 'draw'
	end
  end

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users
end