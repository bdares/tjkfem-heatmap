# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Heatmap::Application.config.secret_key_base = '5e4e796c8347ca4598bfa98c15459272d7a95f3a9c8cc1bd7ded436f3da184236088b6ae44ee28781c801a81eb00d56340644117e264828f7032da72a20a66cc'
