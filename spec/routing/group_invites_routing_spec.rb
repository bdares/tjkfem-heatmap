require "spec_helper"

describe GroupInvitesController do
  describe "routing" do

    it "routes to #index" do
      get("/group_invites").should route_to("group_invites#index")
    end

    it "routes to #new" do
      get("/group_invites/new").should route_to("group_invites#new")
    end

    it "routes to #show" do
      get("/group_invites/1").should route_to("group_invites#show", :id => "1")
    end

    it "routes to #edit" do
      get("/group_invites/1/edit").should route_to("group_invites#edit", :id => "1")
    end

    it "routes to #create" do
      post("/group_invites").should route_to("group_invites#create")
    end

    it "routes to #update" do
      put("/group_invites/1").should route_to("group_invites#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/group_invites/1").should route_to("group_invites#destroy", :id => "1")
    end

  end
end
