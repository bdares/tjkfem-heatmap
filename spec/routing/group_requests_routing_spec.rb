require "spec_helper"

describe GroupRequestsController do
  describe "routing" do

    it "routes to #index" do
      get("/group_requests").should route_to("group_requests#index")
    end

    it "routes to #new" do
      get("/group_requests/new").should route_to("group_requests#new")
    end

    it "routes to #show" do
      get("/group_requests/1").should route_to("group_requests#show", :id => "1")
    end

    it "routes to #edit" do
      get("/group_requests/1/edit").should route_to("group_requests#edit", :id => "1")
    end

    it "routes to #create" do
      post("/group_requests").should route_to("group_requests#create")
    end

    it "routes to #update" do
      put("/group_requests/1").should route_to("group_requests#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/group_requests/1").should route_to("group_requests#destroy", :id => "1")
    end

  end
end
