require "spec_helper"

describe DataCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/data_categories").should route_to("data_categories#index")
    end

    it "routes to #new" do
      get("/data_categories/new").should route_to("data_categories#new")
    end

    it "routes to #show" do
      get("/data_categories/1").should route_to("data_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/data_categories/1/edit").should route_to("data_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/data_categories").should route_to("data_categories#create")
    end

    it "routes to #update" do
      put("/data_categories/1").should route_to("data_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/data_categories/1").should route_to("data_categories#destroy", :id => "1")
    end

  end
end
