class CreateGroupRequests < ActiveRecord::Migration
  def change
    create_table :group_requests do |t|
      t.references :requester, index: true
      t.references :group, index: true
      t.text :message
      t.datetime :accepted
      t.datetime :declined

      t.timestamps
    end
  end
end
