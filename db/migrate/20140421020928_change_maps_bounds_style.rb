class ChangeMapsBoundsStyle < ActiveRecord::Migration
  def change
    remove_column :maps, :latitude
	remove_column :maps, :longitude
	remove_column :maps, :zoom_level
	
	add_column :maps, :bound_north, :float
	add_column :maps, :bound_south, :float
	add_column :maps, :bound_east, :float
	add_column :maps, :bound_west, :float
  end
end
