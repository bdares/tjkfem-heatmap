class CreateDataCategories < ActiveRecord::Migration
  def change
    create_table :data_categories do |t|
      t.string :name
      t.string :unit

      t.timestamps
    end
  end
end
