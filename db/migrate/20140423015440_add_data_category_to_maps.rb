class AddDataCategoryToMaps < ActiveRecord::Migration
  def change
    add_reference :maps, :data_category, index: true
  end
end
