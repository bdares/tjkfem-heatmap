class CreateGroupInvites < ActiveRecord::Migration
  def change
    create_table :group_invites do |t|
      t.references :inviter, index: true
      t.references :group, index: true
      t.references :invitee, index: true
      t.text :message
      t.datetime :accepted
      t.datetime :declined

      t.timestamps
    end
  end
end
