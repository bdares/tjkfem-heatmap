class AddDefaultDateIntervalToMap < ActiveRecord::Migration
  def change
    add_column :maps, :date_interval_count, :integer
	add_column :maps, :date_interval_unit, :string
  end
end
