class RenameMapRadiusToZoomLevel < ActiveRecord::Migration
  def change
    rename_column :maps, :radius, :zoom_level
  end
end
