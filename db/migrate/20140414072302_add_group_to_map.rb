class AddGroupToMap < ActiveRecord::Migration
  def change
    add_column :maps, :group_id, :integer, index: true
  end
end
