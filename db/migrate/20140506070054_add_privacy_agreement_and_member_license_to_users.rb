class AddPrivacyAgreementAndMemberLicenseToUsers < ActiveRecord::Migration
  def change
    add_column :users, :privacy_agreement, :boolean
	add_column :users, :member_license, :boolean
	
	User.all.each do |user|
	  user.privacy_agreement = true
	  user.member_license = true
	  user.save
	end
  end
end
