class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.string :name
      t.references :user, index: true
      t.float :latitude
      t.float :longitude
      t.float :radius
      t.string :address
      t.text :description
      t.string :unit

      t.timestamps
    end
  end
end
