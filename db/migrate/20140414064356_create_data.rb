class CreateData < ActiveRecord::Migration
  def change
    create_table :data do |t|
      t.references :user, index: true
      t.references :map, index: true
      t.float :latitude
      t.float :longitude
      t.string :address
      t.string :measurement
      t.float :value
      t.datetime :measure_time

      t.timestamps
    end
  end
end
