class AddVisualToData < ActiveRecord::Migration
  def self.up
    add_attachment :data, :visual
  end
  
  def self.down
    remove_attachment :data, :visual
  end
end
