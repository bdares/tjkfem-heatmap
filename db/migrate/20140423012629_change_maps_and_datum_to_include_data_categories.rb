class ChangeMapsAndDatumToIncludeDataCategories < ActiveRecord::Migration
  def change
    add_column :data, :data_category_id, :integer, index: true
	remove_column :maps, :unit
  end
end
