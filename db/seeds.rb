﻿# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'ROLES'
%w[admin user].each do |role|
  Role.find_or_create_by_name(role)
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.find_or_create_by_email name: 'admin', email: 'lee_dahyun@naver.com', password: 'default_password', password_confirmation: 'default_password', privacy_agreement: true, member_license: true
puts 'user: ' << user.name
user.add_role :admin

puts 'DATA CATEGORIES'
%w[온도 로드킬 쓰래기].each do |data_category|
  DataCategory.find_or_create_by_name(data_category)
  puts 'data category: ' << data_category
end
