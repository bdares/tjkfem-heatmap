json.array!(@group_requests) do |group_request|
  json.extract! group_request, :id, :requester_id, :group_id, :message, :accepted, :declined
  json.url group_request_url(group_request, format: :json)
end
