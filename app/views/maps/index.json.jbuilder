json.array!(@maps) do |map|
  json.extract! map, :id, :name, :user_id, :latitude, :longitude, :radius, :address, :description, :unit
  json.url map_url(map, format: :json)
end
