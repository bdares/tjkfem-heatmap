json.array!(@data) do |datum|
  json.extract! datum, :id, :user_id, :map_id, :latitude, :longitude, :address, :measurement, :value, :measure_time
  json.url datum_url(datum, format: :json)
end
