json.array!(@data_categories) do |data_category|
  json.extract! data_category, :id, :name, :unit
  json.url data_category_url(data_category, format: :json)
end
