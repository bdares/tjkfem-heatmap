json.array!(@group_invites) do |group_invite|
  json.extract! group_invite, :id, :inviter_id, :group_id, :invitee_id, :message, :accepted, :declined
  json.url group_invite_url(group_invite, format: :json)
end
