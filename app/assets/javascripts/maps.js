//= require underscore
//= require gmaps/google

var handler = Gmaps.build('Google');

function calculate_bounds(bounds) {
  var north = bounds.getNorthEast().lat();
  var east = bounds.getNorthEast().lng();
  var south = bounds.getSouthWest().lat();
  var west = bounds.getSouthWest().lng();
  
  $('#map_bound_north').val(north);
  $('#map_bound_east').val(east);
  $('#map_bound_south').val(south);
  $('#map_bound_west').val(west);
}

var onmapload = 'uninitialized';

$(function(){
    // initialize google map
    var north = $('#map').data('north');
	var east = $('#map').data('east');
	var south = $('#map').data('south');
	var west = $('#map').data('west');

	handler.buildMap({ provider: {}, internal: {id: 'map'}}, function(){
	  google.maps.event.addListener(handler.getMap(), 'bounds_changed', function(){
	    calculate_bounds(handler.getMap().getBounds());
	  });
	  
	  var northEast = new google.maps.LatLng(north, east);
	  var southWest = new google.maps.LatLng(south, west);
	  var bounds = new google.maps.LatLngBounds(southWest, northEast);
	  
	  handler.getMap().fitBounds(bounds);
	  
	  if (onmapload != 'uninitialized') {
	    onmapload();
	  }
	});
	
	// initialize filters
	$('.toolbar input').change(function(){
	  $(this).closest('form').submit();
	});
	
	$('input.bootstrapswitch').bootstrapSwitch({
	  onSwitchChange: function(){
	   $(this).change();
	  }
	});
    
    $('a.download').click(function(){
      $('input[name="format"]').val($(this).data('format'));
      var params = '?' + $('form').serialize();
      $(this).attr('href', params)
      $('input[name="format"]').val($(this).data('js'));
    });
    
	if($('form.edit_map, form.new_map').length == 0) {
	  resized_window();
    }
});

function on_daterange_change() {
  $('.toolbar input:first').change();
}


// resize map on window resize
function resized_window() {
  $('#map').height($(window).height() - 310);
  google.maps.event.trigger(handler.getMap(), 'resize');
}

// debounce window resize
var doit;
$(function(){
  if($('form.edit_map, form.new_map').length == 0) {
    window.onresize = function(){
      clearTimeout(doit);
      doit = setTimeout(resized_window, 100);
    };
  }
});


