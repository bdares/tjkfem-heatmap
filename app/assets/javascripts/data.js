//= require underscore
//= require gmaps/google

var handler = Gmaps.build('Google');

$(function(){
  // convert address to latitude/longitude
  $('#geocode').click(function(e){
    e.preventDefault();
    
    var address = $('#datum_address').val();
    
    $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=' 
      + encodeURIComponent(address), function(data){
        if (data.results.length > 0) {
          var result = data.results[0];
          var lat = result.geometry.location.lat;
          var lng = result.geometry.location.lng;
          $('#datum_latitude').val(lat);
          $('#datum_longitude').val(lng).change();
        }
    });
  });
  
  // strip units from measurement for numeric value
  $('#datum_measurement').keyup(function(){
    var newval = parseFloat($(this).val());
    newval = isNaN(newval) ? '' : newval;
    $('#datum_value').val(newval);
  });

  // EXIF toggle
  $('input[type="checkbox"][name="use_exif"]').change(function(){
    $('.exif').toggle();
  });
  
  // google maps
  $map = $('#map');
  var north = $map.data('north');
  var east = $map.data('east');
  var south = $map.data('south');
  var west = $map.data('west');
  
  var marker;

  handler.buildMap({ provider: {}, internal: {id: 'map'}}, function(){
    var northEast = new google.maps.LatLng(north, east);
    var southWest = new google.maps.LatLng(south, west);
    var bounds = new google.maps.LatLngBounds(southWest, northEast);
     
    handler.getMap().fitBounds(bounds);
  
    var center = handler.getMap().getCenter();
	
	marker = new google.maps.Marker({
	  position: center,
	  map: handler.getMap(),
	  draggable: true
	});
	
    google.maps.event.addListener(marker, 'drag', function(){
	  $('#datum_latitude').val(marker.position.lat());
	  $('#datum_longitude').val(marker.position.lng());
    });
  });
  
  
  $('#datum_latitude, #datum_longitude').change(function(){
    var lat = $('#datum_latitude').val();
	var lng = $('#datum_longitude').val();
	marker.setPosition(new google.maps.LatLng(lat, lng));
  });
  
});
