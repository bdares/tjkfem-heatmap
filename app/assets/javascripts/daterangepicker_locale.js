﻿moment.lang('ko');

var daterange_locale = {
  applyLabel: '적용',
  cancelLabel: '취소',
  fromLabel: '시작',
  toLabel: '끝',
  weekLabel: '주'
}