class DataCategory < ActiveRecord::Base
  has_many :data
  validates_uniqueness_of :name
  
  def to_s
    name
  end
end
