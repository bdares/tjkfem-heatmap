class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
	elsif user.has_role? :user
	  can :read, :all
	  can [:read, :create], GroupRequest do |group_request|
	    group_request.requester.nil? || group_request.requester == user
	  end
	  can [:read, :accept, :decline], GroupInvite do |group_invite|
	    group_invite.invitee == user
	  end
	  can :add_data, Map do |map|
	    map.group && map.group.users.exists?(user)
	  end
	  can :see_email, User do |_user|
	    user == _user
	  end
	else 
	  
    end
	
	can :invite, Group do |group|
	  group.user == user
	end
	
	can :request, Group do |group|
	  ! user.groups.include? (group)
	end
	
	can :leave, Group do |group|
	  user.groups.include? (group)
	end
    
    can :create, GroupRequest
	
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
