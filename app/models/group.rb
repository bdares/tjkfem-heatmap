class Group < ActiveRecord::Base
  belongs_to :user
  has_many :user_groups
  has_many :users, through: :user_groups
  has_many :group_invites
  has_many :group_requests
  has_many :maps
  
  after_save do |group|
    new_owner = group.user
	new_owner.groups << group if !new_owner.groups.include?(group)
	new_owner.save
  end
  
  def to_s
    name
  end
end
