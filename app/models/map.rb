﻿class Map < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  belongs_to :data_category
  has_many :data, dependent: :delete_all
  validates_inclusion_of :date_interval_unit, in: %w[day week month year], message: '허용되지 않은 단위 입니다'
  
  def bounded_data(dates, category_filter)
    data.
	  where(measure_time: dates[0]..dates[1].next_day).
	  where(data_category_id: category_filter)
  end
  
  def last_data_entry_date
    last_data_entry = data.order(:measure_time).last
	if last_data_entry
	  last_data_entry.measure_time.to_date
	else
	  self.updated_at.to_date
	end
  end
  
  def display_interval_begin
    (self.date_interval_count.send self.date_interval_unit).ago.to_date
  end
end
