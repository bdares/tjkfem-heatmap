class User < ActiveRecord::Base
  has_many :user_groups
  has_many :groups, through: :user_groups
  has_many :group_invites
  has_many :group_requests, foreign_key: 'requester_id'
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  after_create :set_default_role
  validates_acceptance_of :privacy_agreement, allow_nil: false, accept: true, on: :create
  validates_acceptance_of :member_license, allow_nil: false, accept: true, on: :create
		 
  def maps
    group_ids = groups.select(:group_id)
    Map.where(group_id: group_ids)
  end
  
  def to_s
    self.name
  end
  
  def set_default_role
    self.add_role :user if self.roles.length == 0
  end
end
