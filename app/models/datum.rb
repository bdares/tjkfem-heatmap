class Datum < ActiveRecord::Base
  belongs_to :user
  belongs_to :map
  belongs_to :data_category
  has_attached_file :visual, styles: {medium: '200x200>', thumb: '50x50>'}, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :visual, content_type: /\Aimage\/.*\Z/
  validates :data_category_id, presence: true
  
  after_save :load_exif
  
  def load_exif
    return if @run
    return if !visual.file?
    exif = EXIFR::JPEG.new(visual.path.gsub('//', '/'))
	self.measure_time = exif.date_time if exif.date_time
	self.latitude = exif.gps_latitude.to_f if exif.gps_latitude
	self.longitude = exif.gps_longitude.to_f if exif.gps_longitude
	@run = true
	File.delete(self.visual.path)
	save
	true
  end
end
