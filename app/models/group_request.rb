class GroupRequest < ActiveRecord::Base
  belongs_to :requester, class_name: "User"
  belongs_to :group
  
  def fresh
    accepted.nil? && declined.nil?
  end
end
