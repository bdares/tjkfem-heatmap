class GroupRequestsController < ApplicationController
  before_action :set_group_request, only: [:show, :edit, :update, :destroy, :accept, :decline]

  # GET /group_requests
  # GET /group_requests.json
  def index
    authorize! :index, @group_requests
    if can? :manage, GroupRequest
	  @group_requests = GroupRequest.scoped
	  @group_requests = @group_requests.where(accepted:nil, declined:nil) if params[:fresh]
	else
	  @group_requests = current_user.group_requests
	end
  end

  # GET /group_requests/1
  # GET /group_requests/1.json
  def show
    authorize! :show, @group_request
  end

  # GET /group_requests/new
  def new
    @group_request = GroupRequest.new
	group = Group.find(params[:group_id]) if params[:group_id]
  end

  # GET /group_requests/1/edit
  def edit
  end

  # POST /group_requests
  # POST /group_requests.json
  def create
    @group_request = GroupRequest.new(group_request_params)
	@group_request.requester = current_user
	
	authorize! :create, @group_request

    respond_to do |format|
      if @group_request.save
        format.html { redirect_to @group_request.group, notice: t('notice.created', model:GroupRequest) }
        format.json { render action: 'show', status: :created, location: @group_request }
      else
        format.html { render action: 'new' }
        format.json { render json: @group_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_requests/1
  # PATCH/PUT /group_requests/1.json
  def update
    authorize! :update, @group_request
    respond_to do |format|
      if @group_request.update(group_request_params)
        format.html { redirect_to @group_request, notice: t('notice.updated', model:GroupRequest) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @group_request.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def accept
    authorize! :accept, @group_request
    new_member = @group_request.requester
	group = @group_request.group
	if !new_member.groups.include? (group)
	  new_member.groups << group
	  new_member.save
	end
	@group_request.requester.group_requests.where(accepted:nil,declined:nil).update_all(accepted:Time.now)
	redirect_to context_return_url
  end
  
  def decline
    authorize! :decline, @group_request
	@group_request.requester.group_requests.
	  where(accepted:nil,declined:nil).
	  update_all(declined:Time.now)
	redirect_to context_return_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_request
      @group_request = GroupRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_request_params
      params.require(:group_request).permit(:group_id, :message)
    end
	
	def context_return_url
	  if params[:fresh]
	    group_requests_path(fresh:true)
	  else
	    group_requests_path
	  end
	end
end
