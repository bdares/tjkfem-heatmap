class MapsController < ApplicationController
  before_action :set_map, only: [:show, :edit, :update, :destroy]
  
  helper_method :marker_type, :daterange, :category_filter

  # GET /maps
  # GET /maps.json
  def index
    @maps = Map.all
  end

  # GET /maps/1
  # GET /maps/1.json
  def show
    params[:format] ||= 'html'
	@hash = Gmaps4rails.build_markers (@map.bounded_data(dates, category_filter)) do |datum, marker |
	  marker.lat datum.latitude
	  marker.lng datum.longitude
	  marker.title datum.value.to_s
      puts params[:format]
      if %w[js html].include? params[:format]
	    marker.infowindow render_to_string(partial: '/data/infowindow', locals: {datum: datum})
	  end
      
	  if marker_type == 'visual'
	    marker.picture ({'url' => datum.visual.url(:thumb), 'width' => 20, 'height' => 20})
	  end
	end
	
	respond_to do |format|
	  format.html
	  format.js
      format.json {send_data @map.to_json(include: :data), filename: @map.name + '.json'}
      format.xml {send_data @map.to_xml(include: :data), filename: @map.name + '.xml'}
      format.xls {send_data @map.to_xls, filename: @map.name + '.xls'}
      format.xlsx {send_data @map.to_xlsx, filename: @map.name + '.xlsx'}
      format.csv {send_data @map.bounded_data(dates, category_filter).to_csv, filename: @map.name + '.csv'}
	end
  end

  # GET /maps/new
  def new
    @map = Map.new(bound_north: 36.459895652096876,
	bound_east: 127.51000106334686,
	bound_south: 36.23868108316989,
	bound_west: 127.23534286022186)
  end

  # GET /maps/1/edit
  def edit
  end

  # POST /maps
  # POST /maps.json
  def create
    @map = Map.new(map_params)
	@map.user = current_user

    respond_to do |format|
      if @map.save
        format.html { redirect_to @map, notice: 'Map was successfully created.' }
        format.json { render action: 'show', status: :created, location: @map }
      else
        format.html { render action: 'new' }
        format.json { render json: @map.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /maps/1
  # PATCH/PUT /maps/1.json
  def update
    respond_to do |format|
      if @map.update(map_params)
        format.html { redirect_to @map, notice: 'Map was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @map.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maps/1
  # DELETE /maps/1.json
  def destroy
    @map.destroy
    respond_to do |format|
      format.html { redirect_to maps_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_map
      @map = Map.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def map_params
      params.require(:map).permit(:name, :bound_north, :bound_south, :bound_east, :bound_west, :address, :description, :unit, :group_id, :date_interval_unit, :date_interval_count, :data_category_id)
    end
	
	def marker_type
	  params[:marker] || 'pin'
	end
	
	def daterange
	  return @daterange if @daterange
	  @daterange ||= dates.map{|x| x.strftime("%Y.%m.%d")}.join ' - '
	end
	
	def dates
	  return @dates if @dates
	  if params[:daterange] && params[:daterange].length > 0
	    date_a = params[:daterange].
	      split('-').
		  map{|x| x.strip}.
		  map{|x| Date.parse(x, "%Y.%m.%d")}
	  else
	    date_a = [@map.display_interval_begin, Date.today]
	  end
	  @dates = date_a
	end
	
	def category_filter
	  if params[:data_category] && params[:data_category].length > 0
	    params[:data_category].map{|x| x.to_i} << 'asdf'
	  else
	    [(@map.data_category_id || default_category.id)]
	  end
	end
	
	def default_category
	  if @map.data.length == 0
	    DataCategory.first
	  else
	    @map.data.order(:measure_time).last.data_category
	  end
	end
end
