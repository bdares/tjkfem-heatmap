class DataCategoriesController < ApplicationController
  before_action :set_data_category, only: [:show, :edit, :update, :destroy]

  # GET /data_categories
  # GET /data_categories.json
  def index
    @data_categories = DataCategory.all
	authorize! :index, @data_categories
  end

  # GET /data_categories/1
  # GET /data_categories/1.json
  def show
    authorize! :show, @data_category
  end

  # GET /data_categories/new
  def new
	@data_category = DataCategory.new
	authorize! :new, @data_category
  end

  # GET /data_categories/1/edit
  def edit
    authorize! :edit, @data_category
  end

  # POST /data_categories
  # POST /data_categories.json
  def create
    @data_category = DataCategory.new(data_category_params)
	authorize! :create, @data_category

    respond_to do |format|
      if @data_category.save
        format.html { redirect_to @data_category, notice: 'Data category was successfully created.' }
        format.json { render action: 'show', status: :created, location: @data_category }
      else
        format.html { render action: 'new' }
        format.json { render json: @data_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /data_categories/1
  # PATCH/PUT /data_categories/1.json
  def update
    authorize! :update, @data_category
    respond_to do |format|
      if @data_category.update(data_category_params)
        format.html { redirect_to @data_category, notice: 'Data category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @data_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data_categories/1
  # DELETE /data_categories/1.json
  def destroy
    authorize! :destroy, @data_category
    @data_category.destroy
    respond_to do |format|
      format.html { redirect_to data_categories_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_data_category
      @data_category = DataCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def data_category_params
      params.require(:data_category).permit(:name, :unit)
    end
end
